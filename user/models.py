import os
from uuid import uuid4

from django.contrib.auth.models import (AbstractBaseUser, AbstractUser,
                                        PermissionsMixin,
                                        UnicodeUsernameValidator, UserManager)
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import F

from .tasks import send_sms

# Functions


def profile_uplaod_dir(instance, filename):
    """
    Define user profile upload path, each user will have a directory for their profiles,
    so we does not loss any data.
    """
    # TODO: Change to uid
    return os.path.join("profiles", str(instance.id), filename)


# Managers

class CustomUserManager(UserManager):
    """
    override some usermanager method according to ower user custom user model.
    """

    def _create_user(self, phone, password, **extra_fields):
        """
        Create and save a user with the given phone and password.
        """
        if not phone:
            raise ValueError('The given phone must be set')
        user = self.model(phone=phone, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, phone, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(phone, password, **extra_fields)

    def create_superuser(self, phone, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(phone, password, **extra_fields)


# Models

class BaseModel(models.Model):
    """
    This is a base model that all models inheritance this.
    """
    uid = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class User(AbstractBaseUser, BaseModel, PermissionsMixin):
    """
    A custom user model that just cantain required fields,
    django default fields removed
    """

    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=150, blank=True)
    phone = models.CharField(
        _("Phone Number"), max_length=15, unique=True, db_index=True)

    referral = models.ForeignKey("self", on_delete=models.SET_NULL, related_name="invitions", null=True,
                                 blank=True, verbose_name=_("Refferal"))

    birth_date = models.DateField(_("Birth Day"), null=True, blank=True)

    image = models.ImageField(
        _("Profile"), upload_to=profile_uplaod_dir, null=True, blank=True)

    # user status
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_(
            'Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )

    objects = CustomUserManager()

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def sms_user(self, message):
        """Send an SMS to this user."""
        send_sms.delay(self.phone, message)

    @property
    def id(self):
        """
        return uid as a string
        """
        return str(self.uid)


class UserAddress(BaseModel):
    """
    Model that cantains users address with latitude and langitude
    """
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="address")
    address = models.CharField(_("Address"), max_length=512)
    latitude = models.FloatField(_("Latitude"), null=True, blank=True)
    longitude = models.FloatField(_("Longitude"), null=True, blank=True)

    class Meta:
        verbose_name = _("User Address")
        verbose_name_plural = _("User Addresses")

    # Magic methods
    def __str__(self):
        return f"<address of user: {self.user_id}>"


class UserScoreTransaction(BaseModel):
    """
    """

    TYPES = (
        (0, _("Profile")),
        (1, _("Invations"))
    )

    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="score_transactions")
    type = models.PositiveSmallIntegerField(choices=TYPES)
    points = models.PositiveIntegerField()

    class Meta:
        verbose_name = _("User Score Transaction")
        verbose_name_plural = _("User Score Transactions")

    # Magic Methods

    def __str__(self):
        return f"<user: {self.user_id}, type: {self.type}>"


class UserScore(BaseModel):
    """
    """
    TYPES = (
        (0, _("Profile")),
        (1, _("Invations"))
    )
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="score")
    type = models.PositiveSmallIntegerField(choices=TYPES)
    points = models.PositiveIntegerField()

    class Meta:
        verbose_name = _("User Score")
        verbose_name_plural = _("User Scores")
        unique_together = ("user", "type")

    # Magic Methods

    def __str__(self):
        return f"<score of user: {self.user_id}>"


# Signals

@receiver(post_save, sender=UserScoreTransaction)
def update_score(sender, instance, created, **kwargs):
    """
    signal for auto update UserScore when user score transaction added
    """
    if not created:
        return
    user_score = UserScore.objects.filter(
        user=instance.user, type=instance.type)
    if user_score.exists():
        user_score.update(points=F("points")+instance.points)
    else:
        UserScore.objects.create(
            user=instance.user, type=instance.type, points=instance.points)
