from rest_framework import serializers
from user.models import *


class AddressSerailizer(serializers.ModelSerializer):
    class Meta:
        model = UserAddress
        fields = "__all__"


class UserScoreTransactionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserScoreTransaction
        fields = "__all__"
