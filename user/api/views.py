import random

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import update_last_login
from django.utils.translation import ugettext_lazy as _
from rest_framework import status
from rest_framework.mixins import ListModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet, ModelViewSet
from rest_framework_jwt.settings import api_settings
from django_filters.rest_framework import DjangoFilterBackend

from sabzbin.redis import SabzRedis
from user.models import User, UserAddress
from user.tasks import send_sms

from .paginations import AddressSetPagination
from .permissions import IsResouceOwner
from .serializers import *

JWT_PAYLOAD_HANDLER = api_settings.JWT_PAYLOAD_HANDLER
JWT_ENCODE_HANDLER = api_settings.JWT_ENCODE_HANDLER


class GetOTP(APIView):
    """
    Class With just a post method for generate and send otp code.
    otp code will save on redis to a while.
    """

    def post(self, request, *args, **kwargs):
        phone = request.data.get("phone", False)
        # Check phone number exists
        if not phone:
            return Response({"errors": [{"message": _("Phone Number required!"), "source": "phone"}]},
                            status=status.HTTP_400_BAD_REQUEST)

        # Get user with that phone number
        if not User.objects.filter(phone=phone).exists():
            return Response({"errors": [{"message": _("Invalid phone number!"), "source": "phone"}]},
                            status=status.HTTP_400_BAD_REQUEST)
        user = User.objects.get(phone=phone)
        # Generate OTP Code
        otp = random.randint(10000, 99999)
        SabzRedis().set_otp(otp, user.id)

        # TODO: modify sms panel in the task of send sms
        #user.sms_user(f"Your OTP Code: {otp}")

        return Response({"code": 200, "message": _("OTP code sent successfully.")})


class GetToken(APIView):
    """
    class for check and generate access token with JWT.
    """

    def post(self, request, *args, **kwargs):
        phone = request.data.get("phone", False)
        otp = request.data.get("OTP", False)
        if not all((otp, phone)):
            return Response({"errors": [{"message": _("Phone Number and OTP code required!"), "source": "phone, OTP"}]},
                            status=status.HTTP_400_BAD_REQUEST)

        if not User.objects.filter(phone=phone).exists():
            return Response({"errors": [{"message": _("Invalid phone number!"), "source": "phone"}]},
                            status=status.HTTP_400_BAD_REQUEST)

        user = User.objects.get(phone=phone)

        user_id = SabzRedis().get_otp(otp)
        if not user_id:
            return Response({"errors": [{"message": _("Invalid OTP code!")}]},
                            status=status.HTTP_400_BAD_REQUEST)

        if not user_id.decode("utf-8") == user.id:
            return Response({"errors": [{"message": _("Invalid OTP code!")}]},
                            status=status.HTTP_400_BAD_REQUEST)

        payload = JWT_PAYLOAD_HANDLER(user)
        jwt_token = JWT_ENCODE_HANDLER(payload)
        update_last_login(None, user)

        return Response({
            "token": jwt_token
        })


class AddressApiView(ModelViewSet, LoginRequiredMixin):
    model = UserAddress
    serializer_class = AddressSerailizer
    pagination_class = AddressSetPagination
    permission_classes = (IsAuthenticated, IsResouceOwner)

    def get_queryset(self):
        qs = UserAddress.objects.filter(user=self.request.user)
        return qs

    def create(self, request, *args, **kwargs):
        request.data['user'] = request.user.id
        return super(AddressApiView, self).create(request, args, kwargs)


class UserScoreTransactionView(GenericViewSet, ListModelMixin):
    model = UserScoreTransaction
    serializer_class = UserScoreTransactionsSerializer
    permission_classes = (IsAuthenticated, )
    filter_backends = (DjangoFilterBackend, )
    filterset_fields = ("type",)

    def get_queryset(self):
        qs = self.model.objects.filter(user=self.request.user)
        return qs
