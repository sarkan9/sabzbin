from django.urls import path
from .api import views
from rest_framework_simplejwt.views import TokenRefreshView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register("address", views.AddressApiView, "address")
router.register("score", views.UserScoreTransactionView, "score")

urlpatterns = [
    path("otp/", views.GetOTP.as_view(), name="get-otp"),
    path("token/", views.GetToken.as_view(), name="get-token"),
    path("token/refresh/", TokenRefreshView.as_view(), name="refresh-token"),
]

urlpatterns += router.urls
