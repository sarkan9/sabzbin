from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import *
from django.utils.translation import ugettext_lazy as _


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('phone', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', "birth_date",
                                         "image")
                              }),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (_('Important dates'), {'fields': ('last_login', )}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('phone', 'password1', 'password2'),
        }),
    )

    list_display = ('phone', 'first_name', 'last_name', 'is_staff')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups')
    search_fields = ('phone', 'first_name', 'last_name')
    ordering = ('phone',)


@admin.register(UserScoreTransaction)
class UserScoreTransactionsAdmin(admin.ModelAdmin):
    list_display = ("user", "type", "points")
    list_filter = ("type", )
    search_fields = ("user", )

    # This will help you to disable delete functionaliyt

    def has_delete_permission(self, request, obj=None):
        return False

    # This will help you to disable change functionality
    def has_change_permission(self, request, obj=None):
        return False


@admin.register(UserScore)
class UserScoreAdmin(UserScoreTransactionsAdmin):
    # This will help you to disbale add functionality
    def has_add_permission(self, request):
        return False


admin.site.register(UserAddress)
