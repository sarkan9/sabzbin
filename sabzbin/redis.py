from django.conf import settings
import redis


class SabzRedis(object):
    """
    """
    otp_prefix = "OTP:%s"

    __instance = None

    def __new__(cls, *args, **kwargs):
        if cls.__instance == None:
            cls.__instance = super(SabzRedis, cls).__new__(
                SabzRedis, *args, **kwargs)
        return cls.__instance

    def __init__(self, *args, **kwargs):
        pool = redis.ConnectionPool(
            host=settings.REDIS['host'],
            db=settings.REDIS['db'],
            port=settings.REDIS['port'],
            password=settings.REDIS['password']
        )

        self._conn = redis.Redis(connection_pool=pool, decode_responses=True)
        self._pipe = self._conn.pipeline()

    def execute(self):
        return self._pipe.execute()

    def set_otp(self, otp, user_id):
        self._conn.set(self.otp_prefix % str(otp), user_id,
                       settings.REDIS_OTP_EXPIRE_TIME)

    def get_otp(self, otp):
        return self._conn.get(self.otp_prefix % str(otp))
